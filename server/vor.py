import uuid

from flask import Flask, jsonify, request
from flask_cors import CORS


# configuration
DEBUG = True

# instantiate the vor
vor = Flask(__name__)
vor.config.from_object(__name__)

# enable CORS
CORS(vor)

AGREEMENTS = [
    {
        'id': uuid.uuid4().hex,
        'partners': 'Jack Kerouac',
    },
    {
        'id': uuid.uuid4().hex,
        'partners': 'J. K. Rowling',
    },
    {
        'id': uuid.uuid4().hex,
        'partners': 'Dr. Seuss',
    }
]


@vor.route('/agreements', methods=['GET', 'POST'])
def all_agreements():
    response_object = {'status': 'success'}
    if request.method == 'POST':
        post_data = request.get_json()
        AGREEMENTS.append({
            'id': uuid.uuid4().hex,
            'partners': post_data.get('partners'),
            'date': post_data.get('date'),
            'data': post_data.get('data'),
            'signatures': [],
        })
        response_object['message'] = 'Agreement added!'
    else:
        response_object['agreements'] = AGREEMENTS
    return jsonify(response_object)


@vor.route('/agreements/<agreement_id>', methods=['GET', 'POST', 'PUT'])
def single_agreement(agreement_id):
    response_object = {'status': 'success'}
    if request.method == 'GET':
        # TODO: refactor to a lambda and filter
        return_agreement = ''
        for agreement in AGREEMENTS:
            if agreement['id'] == agreement_id:
                return_agreement = agreement
        response_object['agreement'] = return_agreement
    elif request.method == 'POST':
        post_data = request.get_json()
        for agreement in AGREEMENTS:
            if agreement['id'] == agreement_id:
                agreement_data = agreement['data']
                agreement_partners = agreement['partners']
        AGREEMENTS.append({
            'id': uuid.uuid4().hex,
            'parentId': agreement_id,
            'partners': agreement_partners,
            'date': post_data.get('date'),
            'data': agreement_data,
        })
        response_object['message'] = 'Agreement updated!'
    elif request.method == 'PUT':
        post_data = request.get_json()
        for agreement in AGREEMENTS:
            if agreement['id'] == agreement_id:
                if not any(d.get('partnerId', None) == '0' for d in agreement.setdefault('signatures',[])):
                    agreement['signatures'].append({
                        'signature': post_data.get('signature'),
                        'partnerId': '0'
                    })
                    response_object['message'] = 'Agreement signed!'
                else: response_object['message'] = 'Agreement already signed!'
    return jsonify(response_object)


if __name__ == '__main__':
    vor.run(host='0.0.0.0', port='5001')
