import Vue from 'vue';
import Router from 'vue-router';
import Agreements from '@/components/Agreements';

Vue.use(Router);

export default new Router({
  routes: [
    {
      path: '/',
      name: 'Agreements',
      component: Agreements,
    },
  ],
  mode: 'hash',
});
